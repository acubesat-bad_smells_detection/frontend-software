import { Directive, ViewContainerRef } from '@angular/core';
import { DataserviceService } from 'src/app/services/dataservice.service';
import { CommandInfo, CustomTelecommandResponse } from 'src/app/Interfaces/Telecommands';
import { TelecommandItemComponent } from '../telecommand-item/telecommand-item.component';
import { TelecommandResponseComponent } from '../telecommand-response/telecommand-response.component';

@Directive({
  selector: '[appCreateTcItem]'
})
export class CreateTcItemDirective {

  yamcsInstanceName = "OBC";

  constructor(private dataService: DataserviceService, private viewRef: ViewContainerRef) { 
    this.dataService.receiveAndMakeTC().subscribe((tc) => this.makeTC(tc));
    this.dataService.directiveDestroyTC().subscribe((tcResponse) => this.destroyTcRef(tcResponse))
  }

  makeTC(tc: CommandInfo): void{
    this.viewRef.remove();
    const currentTC = this.viewRef.createComponent(TelecommandItemComponent);
    currentTC.instance.currentTC = tc;
  }

  destroyTcRef(tcResponse: CustomTelecommandResponse): void{
    this.viewRef.remove();
    /**
     * Remove these 2 lines below if you do not want to create the tc response component
     */
    if(tcResponse.commandResponse !== undefined){
      const currentTcHistory = this.viewRef.createComponent(TelecommandResponseComponent);
      currentTcHistory.instance.response = tcResponse.commandResponse;
      currentTcHistory.instance.yamcsInstanceName = tcResponse.yamcsInstanceName;
    }
  }

  emptyDirective(): void{
    this.viewRef.remove();
  }

}
